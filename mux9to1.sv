/*9-1 mux (variable width)
out = a0 if sel == 1'b0000
out = a1 if sel == 1'b0001
out = a2 if sel == 1'b0010
out = a3 if sel == 1'b0011
out = a4 if sel == 1'b0100
out = a5 if sel == 1'b0101
out = a6 if sel == 1'b0110
out = a7 if sel == 1'b0111
out = a8 if sel == 1'b1000
*/
module mux9to1 #(parameter width = 128)
(
	input logic [3:0] sel,
	input logic [width-1:0] a0, a1, a2, a3, a4, a5, a6, a7, a8,
	output logic [width-1:0] out
);

always_comb
begin
	if (sel == 4'b0000)
		out = a0;
	else if (sel == 4'b0001)
		out = a1;
	else if (sel == 4'b0010)
		out = a2;
	else if (sel == 4'b0011)
		out = a3;
	else if (sel == 4'b0100)
		out = a4;
	else if (sel == 4'b0101)
		out = a5;
	else if (sel == 4'b0110)
		out = a6;
	else if (sel == 4'b0111)
		out = a7;
	else
		out = a8;
end

endmodule : mux9to1