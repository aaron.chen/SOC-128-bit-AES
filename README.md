# SOC-128-bit-AES
(implementation of Lab 5 for ECE 385)
### By Aaron Chen, Shruti Chanumolu

Implementation of 128-AES using SOC / systemverilog. Encryption in C / Decryption in SystemVerilog.

In this lab we designed a 128-bit AES (Advanced encryption standard) using system verilog as an IP (Intellectual Property) core.  This lab is divided into two parts described as follows.

In Part 1,  we implemented a 128-bit AES encryption algorithm in C, on the software IP core and then ran the algorithm on the NIOS II-e processor. We wrote the AddroundKey, Subbyte, Shiftrow, Mixcolumn functions and used them sequentially in the AES function. In addition, we designed a Avalon-MM Interface to display our cipher key on the hex displays.

In Part 2,  we implemented a 128-bit AES decryption in SystemVerilog and incorporated it with  software interface through the avalon AES interface. We also designed a state machine to transition through the different AES modules, as well as to incorporate the capabilities to initialize, run, reset, and flag the completion of the AES decryption algorithm.

The NIOS II is an IP based 32-bit CPU which can programmed using a high level language such as C. It embeds the the software interface which is incorporated with the decryption IP core, to form a complete system on chip (SoC). Through the software interface provided by the NIOS processor, our circuit will be able to decrypt the input data using the provided cipher key, and reconstruct the original message. 

lab9_top.sv should be set as the top-level module.

### State Diagram

<img src="state_diagram.png" alt="state_diagram" />

An annotated simulation can be found in the annotated_simulation folder.
