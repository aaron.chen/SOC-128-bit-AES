/*4-1 mux (variable width)
out = a0 if sel == 1'b0000
out = a1 if sel == 1'b0001
out = a2 if sel == 1'b0010
out = a3 if sel == 1'b0011
*/
module mux4to1 #(parameter width = 128)
(
	input logic [1:0] sel,
	input logic [width-1:0] a0, a1, a2, a3,
	output logic [width-1:0] out
);

always_comb
begin
	if (sel == 2'b00)
		out = a0;
	else if (sel == 2'b01)
		out = a1;
	else if (sel == 2'b10)
		out = a2;
	else
		out = a3;
end

endmodule : mux4to1